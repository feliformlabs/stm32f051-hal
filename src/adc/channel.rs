use hal::adc::Channel;

use super::super::gpio::gpioa::{PA0, PA1, PA2, PA3, PA4, PA5, PA6, PA7};
use super::super::gpio::gpiob::{PB0, PB1};
use super::super::gpio::gpioc::{PC0, PC1, PC2, PC3, PC4, PC5};
use super::super::gpio::Analog;

use super::Adc;

/// Channel number type
pub type ChannelID = u8;

impl<Mode> Channel<Adc<Mode>> for PC0<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        10
    }
}
impl<Mode> Channel<Adc<Mode>> for PC1<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        11
    }
}
impl<Mode> Channel<Adc<Mode>> for PC2<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        12
    }
}
impl<Mode> Channel<Adc<Mode>> for PC3<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        13
    }
}
impl<Mode> Channel<Adc<Mode>> for PC4<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        14
    }
}
impl<Mode> Channel<Adc<Mode>> for PC5<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        15
    }
}
impl<Mode> Channel<Adc<Mode>> for PA0<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        0
    }
}
impl<Mode> Channel<Adc<Mode>> for PA1<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        1
    }
}
impl<Mode> Channel<Adc<Mode>> for PA2<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        2
    }
}
impl<Mode> Channel<Adc<Mode>> for PA3<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        3
    }
}
impl<Mode> Channel<Adc<Mode>> for PA4<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        4
    }
}
impl<Mode> Channel<Adc<Mode>> for PA5<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        5
    }
}
impl<Mode> Channel<Adc<Mode>> for PA6<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        6
    }
}
impl<Mode> Channel<Adc<Mode>> for PA7<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        7
    }
}
impl<Mode> Channel<Adc<Mode>> for PB0<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        8
    }
}
impl<Mode> Channel<Adc<Mode>> for PB1<Analog> {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        9
    }
}

/// Internal temperature sensor channel
pub struct TemperatureSensor;

impl<Mode> Channel<Adc<Mode>> for TemperatureSensor {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        16
    }
}

/// Internal reference voltage channel
pub struct InternalReference;

impl<Mode> Channel<Adc<Mode>> for InternalReference {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        17
    }
}

/// Battery voltage channel
pub struct BatteryVoltage;

impl<Mode> Channel<Adc<Mode>> for BatteryVoltage {
    type ID = ChannelID;
    fn channel() -> Self::ID {
        18
    }
}

/// Marker for things that can represents sets of channels
pub trait Channels<ADC> {
    /// The data type used to represent a channel set
    ///
    /// This may be a bit mask where a 1 represents an enabled channel.
    type Set;
    /// Returns the set of channels that this value represents
    fn channels() -> Self::Set;
}

/// Channel mask corresponds to the value of ADC_CHSELR
pub type ChannelMask = u32;

/// Single channel
impl<C, Mode> Channels<Adc<Mode>> for C
where
    C: Channel<Adc<Mode>, ID = u8>,
{
    type Set = ChannelMask;

    fn channels() -> Self::Set {
        1u32 << C::channel()
    }
}
/// Two channels
impl<C0, C1, Mode> Channels<Adc<Mode>> for (C0, C1)
where
    C0: Channel<Adc<Mode>, ID = u8>,
    C1: Channel<Adc<Mode>, ID = u8>,
{
    type Set = ChannelMask;
    fn channels() -> Self::Set {
        1u32 << C0::channel() | 1u32 << C1::channel()
    }
}
/// Three channels
impl<C0, C1, C2, Mode> Channels<Adc<Mode>> for (C0, C1, C2)
where
    C0: Channel<Adc<Mode>, ID = u8>,
    C1: Channel<Adc<Mode>, ID = u8>,
    C2: Channel<Adc<Mode>, ID = u8>,
{
    type Set = ChannelMask;
    fn channels() -> Self::Set {
        1u32 << C0::channel() | 1u32 << C1::channel() | 1u32 << C2::channel()
    }
}
/// Four channels
impl<C0, C1, C2, C3, Mode> Channels<Adc<Mode>> for (C0, C1, C2, C3)
where
    C0: Channel<Adc<Mode>, ID = u8>,
    C1: Channel<Adc<Mode>, ID = u8>,
    C2: Channel<Adc<Mode>, ID = u8>,
    C3: Channel<Adc<Mode>, ID = u8>,
{
    type Set = ChannelMask;
    fn channels() -> Self::Set {
        1u32 << C0::channel()
            | 1u32 << C1::channel()
            | 1u32 << C2::channel()
            | 1u32 << C3::channel()
    }
}
// TODO: More channels
