//!
//! An interface to the analog-to-digital converter
//!

use core::marker::PhantomData;

use hal::adc::{Channel, OneShot};
use stm32::{ADC, RCC};
use void::Void;

pub mod channel;

use self::channel::ChannelID;

/// One-shot mode for the ADC
pub struct OneShotMode;

/// The analog-to-digital converter configured in one-shot mode
pub struct Adc<Mode> {
    /// ADC registers
    registers: ADC,
    /// Mode
    mode: PhantomData<Mode>,
}

impl Adc<OneShotMode> {
    /// Configures the ADC in one-shot mode
    pub fn new(registers: ADC) -> Self {
        // NOTE(unsafe) This executes only during initialisation
        let rcc = unsafe { &(*RCC::ptr()) };

        // Enable clock
        rcc.apb2enr.modify(|_, w| w.adcen().set_bit());

        // Reset ADC
        rcc.apb2rstr.modify(|_, w| w.adcrst().set_bit());
        rcc.apb2rstr.modify(|_, w| w.adcrst().clear_bit());

        // Calibrate
        registers.cr.modify(|_, w| w.adcal().set_bit());
        while registers.cr.read().adcal().bit_is_set() {}
        // Caution:
        // ADEN bit cannot be set when ADCAL=1 and during four ADC clock cycles after the ADCAL
        // bit is cleared by hardware (end of calibration).

        // May need to add a small delay

        Adc {
            registers,
            mode: PhantomData,
        }
    }

    /// Enables the ADC
    fn enable(&mut self) {
        // Clear ADRDY (set to 1)
        self.registers.isr.modify(|_, w| w.adrdy().set_bit());
        // Request enable
        self.registers.cr.modify(|_, w| w.aden().set_bit());
        // Wait for ADRDY
        while self.registers.isr.read().adrdy().bit_is_clear() {
            self.registers.cr.modify(|_, w| w.aden().set_bit());
        }
    }

    /// Disables the ADC
    fn disable(&mut self) {
        // Stop any active conversion
        if self.registers.cr.read().adstart().bit_is_set() {
            self.registers.cr.modify(|_, w| w.adstp().set_bit());
            while self.registers.cr.read().adstp().bit_is_set() {}
        }
        // Request disable
        self.registers.cr.modify(|_, w| w.addis().set_bit());
        // Wait for disable
        while self.registers.cr.read().aden().bit_is_set() {}
        // Clear ADRDY (set to 1)
        self.registers.isr.modify(|_, w| w.adrdy().set_bit());
    }
}
impl<Mode> Adc<Mode> {
    /// Deconfigures the ADC and returns its registers
    pub fn free(self) -> ADC {
        // Should already be disabled, don't need to do further deconfiguration
        self.registers
    }
}

impl<WORD, PIN> OneShot<Adc<OneShotMode>, WORD, PIN> for Adc<OneShotMode>
where
    u16: Into<WORD>,
    PIN: Channel<Adc<OneShotMode>, ID = ChannelID>,
{
    type Error = Void;

    fn read(&mut self, _pin: &mut PIN) -> nb::Result<WORD, Self::Error> {
        self.enable();
        // Select channel
        self.registers
            .chselr
            .write(|w| unsafe { w.bits(1u32 << PIN::channel()) });
        // Start conversion
        self.registers.cr.modify(|_, w| w.adstart().set_bit());
        // Wait for conversion to end
        while self.registers.isr.read().eoc().bit_is_clear() {}
        // Read value
        let value = self.registers.dr.read().bits() as u16;

        self.disable();
        Ok(value.into())
    }
}
