//! PWM output using timers

use super::gpio::gpioa::{PA0, PA1, PA15, PA2, PA3, PA5, PA6, PA7};
use super::gpio::gpiob::{PB0, PB1, PB10, PB11, PB3, PB4, PB5};
use super::gpio::gpioc::{PC6, PC7, PC8, PC9};
use super::gpio::{Alternate, AF0, AF1, AF2};
use super::rcc::Clocks;
use super::time::Hertz;

use stm32::{RCC, TIM2, TIM3};

/// Marker for pins that can be used for PWM output
pub trait Pins<Timer> {}

/// PA0 as TIM2_CH1_ETR
impl Pins<TIM2> for PA0<Alternate<AF2>> {}
/// PA1 as TIM2_CH2
impl Pins<TIM2> for PA1<Alternate<AF2>> {}
/// PA2 as TIM2_CH3
impl Pins<TIM2> for PA2<Alternate<AF2>> {}
/// PA3 as TIM2_CH4
impl Pins<TIM2> for PA3<Alternate<AF2>> {}

/// PA5 as TIM2_CH1_ETR
impl Pins<TIM2> for PA5<Alternate<AF2>> {}

/// PA15 as TIM2_CH1_ETR
impl Pins<TIM2> for PA15<Alternate<AF2>> {}

/// PB3 as TIM2_CH2
impl Pins<TIM2> for PB3<Alternate<AF2>> {}
/// PB10 as TIM2_CH3
impl Pins<TIM2> for PB10<Alternate<AF2>> {}
/// PB11 as TIM2_CH4
impl Pins<TIM2> for PB11<Alternate<AF2>> {}

/// PA6 as TIM3_CH1
impl Pins<TIM3> for PA6<Alternate<AF1>> {}
/// PA7 as TIM3_CH2
impl Pins<TIM3> for PA7<Alternate<AF1>> {}

/// PB0 as TIM3_CH3
impl Pins<TIM3> for PB0<Alternate<AF1>> {}
/// PB1 as TIM3_CH4
impl Pins<TIM3> for PB1<Alternate<AF1>> {}
/// PB4 as TIM3_CH1
impl Pins<TIM3> for PB4<Alternate<AF1>> {}
/// PB5 as TIM3_CH2
impl Pins<TIM3> for PB5<Alternate<AF1>> {}

/// PC6 as TIM3_CH1
impl Pins<TIM3> for PC6<Alternate<AF0>> {}
/// PC7 as TIM3_CH2
impl Pins<TIM3> for PC7<Alternate<AF0>> {}
/// PC8 as TIM3_CH3
impl Pins<TIM3> for PC8<Alternate<AF0>> {}
/// PC9 as TIM3_CH4
impl Pins<TIM3> for PC9<Alternate<AF0>> {}


/// Two pins
impl<Timer, P0, P1> Pins<Timer> for (P0, P1)
where
    P0: Pins<Timer>,
    P1: Pins<Timer>,
{
}
/// Three pins
impl<Timer, P0, P1, P2> Pins<Timer> for (P0, P1, P2)
where
    P0: Pins<Timer>,
    P1: Pins<Timer>,
    P2: Pins<Timer>,
{}
/// Four pins
impl<Timer, P0, P1, P2, P3> Pins<Timer> for (P0, P1, P2, P3)
where
    P0: Pins<Timer>,
    P1: Pins<Timer>,
    P2: Pins<Timer>,
    P3: Pins<Timer>,
{}

/// Uses a timer to produce a PWM signal
pub struct PwmOut<TIM, PINS> {
    /// Timer registers
    timer: TIM,
    /// Pins
    pins: PINS,
}

impl<TIM, PINS> PwmOut<TIM, PINS> {
    /// Releases the resources
    pub fn release(self) -> (TIM, PINS) {
        (self.timer, self.pins)
    }
}

impl<PINS> PwmOut<TIM3, PINS>
where
    PINS: Pins<TIM3>,
{
    /// Creates a PWM output that uses timer 3
    ///
    /// The initial duty cycle will be 0% for all channels.
    pub fn tim3<F>(timer: TIM3, pins: PINS, frequency: F, clocks: Clocks) -> Self
    where
        F: Into<Hertz>,
    {
        Self::_tim3(timer, pins, frequency.into(), clocks)
    }

    /// Like tim3, but does not depend on the frequency type (may reduce code size)
    fn _tim3(timer: TIM3, pins: PINS, frequency: Hertz, clocks: Clocks) -> Self {
        // NOTE(unsafe) This executes only during initialisation
        let rcc = unsafe { &(*RCC::ptr()) };

        /* Enable clock */
        rcc.apb1enr.modify(|_, w| w.tim3en().set_bit());

        /* Reset */
        rcc.apb1rstr.modify(|_, w| w.tim3rst().set_bit());
        rcc.apb1rstr.modify(|_, w| w.tim3rst().clear_bit());

        // Calculate clock division
        // The counter clock frequency CK_CNT is equal to f_CK_PSC / (PSC[15:0] + 1)
        // Scale to make it correct
        let frequency = frequency.0 * 32767;
        let input_frequency = clocks.pclk();
        let division = input_frequency.0 / frequency;
        assert!(division > 0, "Desired timer frequency is greater than PCLK");
        assert!(
            division <= 0xffff,
            "Difference between PCLK and desired timer frequency is too high"
        );
        let division = (division - 1) as u16;
        timer.psc.write(|w| unsafe { w.psc().bits(division) });

        // Upcounting mode, set ARR to maximum
        timer.arr.write(|w| unsafe { w.bits(0xffff) });

        // Set PWM mode 1 for all channels
        timer.ccmr1_output.write(|w| unsafe {
            // Channel 1
            // PWM mode 1
            w.oc1m()
                .bits(0b110)
                // Enable preload
                .oc1pe()
                .set_bit()
                // Channel configured as output
                .cc1s()
                .bits(0b00)
                // Channel 2
                // PWM mode 1
                .oc2m()
                .bits(0b110)
                .oc2pe()
                .set_bit()
                .cc2s()
                .bits(0b00)
        });
        timer.ccmr2_output.write(|w| unsafe {
            // Channel 3
            // PWM mode 1
            w.oc3m()
                .bits(0b110)
                // Enable preload
                .oc3pe()
                .set_bit()
                // Channel configured as output
                .cc3s()
                .bits(0b00)
                // Channel 4
                // PWM mode 1
                .oc4m()
                .bits(0b110)
                .oc4pe()
                .set_bit()
                .cc4s()
                .bits(0b00)
        });
        // Enable outputs
        timer.ccer.write(|w| {
            w.cc1e()
                .set_bit()
                .cc2e()
                .set_bit()
                .cc3e()
                .set_bit()
                .cc4e()
                .set_bit()
        });

        // Start counting
        timer.cr1.write(|w| w.cen().set_bit());

        PwmOut { timer, pins }
    }

    /// Sets the duty cycle of output channel 1
    ///
    /// 0 corresponds to 0% duty, and u16::MAX corresponds to 100% duty.
    pub fn set_duty_cycle_1(&mut self, duty_cycle: u16) {
        self.timer
            .ccr1
            .write(|w| w.ccr1().bits(u32::from(duty_cycle)));
    }

    /// Sets the duty cycle of output channel 2
    ///
    /// 0 corresponds to 0% duty, and u16::MAX corresponds to 100% duty.
    pub fn set_duty_cycle_2(&mut self, duty_cycle: u16) {
        self.timer
            .ccr2
            .write(|w| w.ccr2().bits(u32::from(duty_cycle)));
    }

    /// Sets the duty cycle of output channel 3
    ///
    /// 0 corresponds to 0% duty, and u16::MAX corresponds to 100% duty.
    pub fn set_duty_cycle_3(&mut self, duty_cycle: u16) {
        self.timer
            .ccr3
            .write(|w| w.ccr3().bits(u32::from(duty_cycle)));
    }

    /// Sets the duty cycle of output channel 4
    ///
    /// 0 corresponds to 0% duty, and u16::MAX corresponds to 100% duty.
    pub fn set_duty_cycle_4(&mut self, duty_cycle: u16) {
        self.timer
            .ccr4
            .write(|w| w.ccr4().bits(u32::from(duty_cycle)));
    }

    /// Returns the current counter value
    pub fn counter(&self) -> u16 {
        self.timer.cnt.read().bits() as u16
    }
}
